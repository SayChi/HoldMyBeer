from django.shortcuts import render
from rest_framework import viewsets, mixins
from rest_framework.generics import get_object_or_404
from rest_framework.response import Response
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated, IsAdminUser
from CountMyBeer.serializers import *
from CountMyBeer import custom_mixins as c_mixins

# Create your views here.


def index(request):
    return render(request, 'CountMyBeer/index.html')


def overview(request):
    return render(request, 'CountMyBeer/overview.html')


class DrinkViewSet(viewsets.ReadOnlyModelViewSet):
    """
    Simple read only view for the Drink model
    """
    authentication_classes = [SessionAuthentication, BasicAuthentication]
    permission_classes = [IsAuthenticated]

    queryset = Drink.objects.all().order_by('name')
    serializer_class = DrinkSerializer


class FoodViewSet(viewsets.ReadOnlyModelViewSet):
    """
    Simple read only view for the Food model
    """
    authentication_classes = [SessionAuthentication, BasicAuthentication]
    permission_classes = [IsAuthenticated]

    queryset = Food.objects.all().order_by('name')
    serializer_class = FoodSerializer


class ProductViewSet(viewsets.ReadOnlyModelViewSet):
    """
    Simple read only view for the Product model
    """
    authentication_classes = [SessionAuthentication, BasicAuthentication]
    permission_classes = [IsAuthenticated]

    queryset = Product.objects.all().order_by('name')
    serializer_class = ProductPolymorphicSerializer


class UserProductViewSet(c_mixins.UserHasProductQuerySet, viewsets.ReadOnlyModelViewSet):
    """
    Simple read only view for the UserHasProduct model
    """
    authentication_classes = [SessionAuthentication, BasicAuthentication]
    permission_classes = [IsAuthenticated]

    queryset = UserHasProduct.objects.all()
    serializer_class = UserHasProductSerializer


class OverviewUserProductViewSet(viewsets.ReadOnlyModelViewSet):
    """
    Simple read only view for the UserHasProduct model
    """
    authentication_classes = [SessionAuthentication, BasicAuthentication]
    permission_classes = [IsAdminUser]

    queryset = UserHasProduct.objects.all()
    serializer_class = UserHasProductSerializer


class UserViewSet(viewsets.ReadOnlyModelViewSet):
    """
    Simple read only view for the User model
    """
    authentication_classes = [SessionAuthentication, BasicAuthentication]
    permission_classes = [IsAdminUser]

    queryset = User.objects.all()
    serializer_class = UserSerializer


class PaymentViewSet(c_mixins.PaymentQuerySet, viewsets.ReadOnlyModelViewSet):
    """
    Simple read only view for the Payment model
    """
    authentication_classes = [SessionAuthentication, BasicAuthentication]
    permission_classes = [IsAuthenticated]

    queryset = Payment.objects.all()
    serializer_class = PaymentSerializer


class CreatePaymentViewSet(mixins.CreateModelMixin, viewsets.GenericViewSet):
    """
    Simple view for the creation of Payment models
    """
    authentication_classes = [SessionAuthentication, BasicAuthentication]
    permission_classes = [IsAdminUser]

    queryset = Payment.objects.all()
    serializer_class = PaymentSerializer


class AllPaymentsViewSet(viewsets.ReadOnlyModelViewSet):
    """
    Simple read only view for the Payment model
    """
    authentication_classes = [SessionAuthentication, BasicAuthentication]
    permission_classes = [IsAdminUser]

    queryset = Payment.objects.all()
    serializer_class = PaymentSerializer


class UpdateAmount(c_mixins.UserHasProductQuerySet, viewsets.GenericViewSet):
    """
    Simple patch only view to increment the amount of the UserHasProduct model
    """

    authentication_classes = [SessionAuthentication, BasicAuthentication]
    permission_classes = [IsAuthenticated]

    queryset = UserHasProduct.objects.all()
    serializer_class = UserHasProductSerializer

    def partial_update(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance)

        user_product = get_object_or_404(self.queryset, pk=serializer.data['id'])
        user_product.amount += 1
        user_product.save()

        serializer = self.get_serializer(user_product)

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}

        return Response(serializer.data)
