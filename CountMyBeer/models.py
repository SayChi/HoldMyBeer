from django.core.validators import MinValueValidator
from django.db import models
from django.contrib.auth.models import User
from polymorphic.models import PolymorphicModel
from django.db.models.signals import post_save
from django.dispatch import receiver

# Create your models here.
from django.utils.translation import ugettext_lazy as _


class Payment(models.Model):
    amount = models.DecimalField(max_digits=10, decimal_places=2, validators=[MinValueValidator(0)])
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)

    class Meta:
        verbose_name = _('Payment')
        verbose_name_plural = _('Payments')

    def __str__(self):
        return str(self.amount)


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)

    class Meta:
        verbose_name = _('Profile')
        verbose_name_plural = _('Profiles')

    def __str__(self):
        return self.user.username


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    Profile.objects.get_or_create(user=instance)


@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()


class Product(PolymorphicModel):
    """
    Simple abstract class that forms the basis for liquid and solid food items
    """

    class Meta:
        verbose_name = _('Product')
        verbose_name_plural = _('Products')

    name = models.CharField(max_length=50)
    price = models.DecimalField(max_digits=6, decimal_places=2)
    image = models.ImageField(upload_to='food/')
    description = models.CharField(max_length=500)

    def __str__(self):
        return self.name


class Drink(Product):
    """
    Simple model that represents a drink.
    Added the volume of the drink.
    Extends from AbstractFood.
    """
    volume_ml = models.PositiveSmallIntegerField()

    class Meta:
        verbose_name = _('Drink')
        verbose_name_plural = _('Drinks')


class Food(Product):
    """
    Simple model that represents solid food.
    Added the weight of the food.
    Extends from AbstractFood.
    """
    weight_gram = models.PositiveSmallIntegerField()

    class Meta:
        verbose_name = _('Food')
        verbose_name_plural = _('Foods')


class UserHasProduct(models.Model):
    """
    Simple model to track which user had how much from what product.
    """
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    amount = models.PositiveSmallIntegerField(default=0)

    def __str__(self):
        return self.user.username + ' ' + self.product.name

    class Meta:
        unique_together = ('user', 'product')
        verbose_name = _('User has Product')
        verbose_name_plural = _('User has Products')
