from rest_framework import serializers
from CountMyBeer.models import *
from rest_polymorphic.serializers import PolymorphicSerializer
from django.contrib.auth.models import User


class DrinkSerializer(serializers.ModelSerializer):
    class Meta:
        model = Drink
        fields = '__all__'


class FoodSerializer(serializers.ModelSerializer):
    class Meta:
        model = Food
        fields = '__all__'


class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = '__all__'


class ProductPolymorphicSerializer(PolymorphicSerializer):
    model_serializer_mapping = {
        Product: ProductSerializer,
        Drink: DrinkSerializer,
        Food: FoodSerializer
    }


class UserHasProductSerializer(serializers.ModelSerializer):
    product = ProductPolymorphicSerializer()

    class Meta:
        model = UserHasProduct
        fields = '__all__'


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = [
            'id',
            'username',
        ]


class PaymentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Payment
        fields = '__all__'
