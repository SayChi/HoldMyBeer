'use strict';
// Global vars
let drinks = null;
let foods = null;
let payments = [];
let products = {};
let id_name_map = new Map();

const template_product_column = $('<div />', {class: 'col-sm-6 col-md-4 col-lg-3 p-3'});
const template_product_card = $('<div />', {class: 'card text-center bg-light'});
const template_product_img = $('<img />', {class: 'card-img-top p-3'});
const template_product_body = $('<div />', {class: 'card-body'});
const template_product_title = $('<h5 />', {class: 'card-title'});
const template_product_subtitle = $('<h6 />', {class: 'card-subtitle text-muted'});
const template_product_text = $('<p />', {class: 'card-text'});
const template_product_btn = $('<a />', {class: 'btn btn-primary'}); // TODO: Add on click handler

$(document).ready(function () {
    setup_ajax();
    get_data();
});


function get_data() {
    get_products()
        .then(get_payments)
        .then(populate_initial)
}


function setup_ajax() {
    $.ajaxSetup({
        beforeSend: function (xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader('X-CSRFToken', CSRF_TOKEN);
            }
        }
    });
}


function populate_initial() {
    sort_products();
    let product_row = $('#product_row');

    for (const [key, value] of id_name_map.entries()) {
        const prod = products[key].product;
        product_row.append(create_card(prod.image, prod.name, prod.description, prod.price, products[key].amount, products[key].id));
    }
}


function sort_products() {
    // Create temporary array with the mappings
    let a = [];
    for (let k in products) {
        a.push([k, products[k].product.name]);
    }

    // Sort the array on the name of the product
    a.sort((a, b) => a[1].localeCompare(b[1]));

    // Finally create the new sorted map
    id_name_map = new Map(a);
}


function create_card(image, title, text, price, amount, id) {
    let e_column = template_product_column.clone();
    let e_card = template_product_card.clone();
    let e_img = template_product_img.clone();
    let e_body = template_product_body.clone();
    let e_title = template_product_title.clone();
    let e_subtitle = template_product_subtitle.clone();
    let e_text = template_product_text.clone();
    let e_btn = template_product_btn.clone();

    e_img.attr('src', image);
    e_title.text(title + ' €' + price);
    e_subtitle.attr('id', 'total_consumed_' + id);
    e_subtitle.text('Total Consumed: ' + amount);
    e_text.text(text);
    e_btn.text('+');
    e_btn.attr('id', id);
    e_btn.click(increment_amount);

    e_body.append(e_title).append(e_subtitle).append(e_text).append(e_btn);
    e_card.append(e_img).append(e_body);
    e_column.append(e_card);

    return e_column;
}


function update_product(data) {
    const id = data.id;
    // Update the amount for the correct product
    // Assumes the data exists, which should be considering data is the response from the increment_amount api call
    products[id].amount = data.amount;

    // Update the card
    $('#total_consumed_' + id).text('Total Consumed: ' + products[id].amount);
    update_total_cost();
}


function update_total_cost(extra) {
    let cost = extra ? parseFloat(extra) : 0;
    for (const [key, value] of id_name_map.entries()) {
        const prod = products[key].product;
        cost += products[key].amount * prod.price;
    }

    for (let i = 0; i < payments.length; i++) {
        cost -= payments[i].amount
    }

    $('#total_cost').text('Total Cost: €' + cost.toFixed(2));
}


function increment_amount() {
    let e = $(this);
    const id = e.attr('id');

    // Fake increment gui for ease of use
    $('#total_consumed_' + id).text('Total Consumed: ' + (products[id].amount + 1));
    update_total_cost(products[id].product.price);

    patch_increment_amount(id, update_product);
}


function process_products(data) {
    // First clear products, just in case
    products = {};
    // Add products indexed by their id, id is unique
    for (let i = 0; i < data.length; i++) {
        products[data[i].id] = data[i];
    }
}


function patch_increment_amount(id, callback) {
    $.ajax({
        type: 'PATCH',
        url: '/api/v1/update_amount/' + id + '/',
        success: function (data) {
            callback(data);
        },
        fail: function (data) {
            console.log('failed incrementing amount');
        }
    })
}


function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}


function get_drinks() {
    return $.ajax({
        type: 'GET',
        url: '/api/v1/drink',
        success: function (data) {
            drinks = data;
        },
        fail: function () {
            console.log('failed getting drink data');
        }
    })
}

function get_food() {
    return $.ajax({
        type: 'GET',
        url: '/api/v1/food',
        success: function (data) {
            foods = data;
        },
        fail: function () {
            console.log('failed getting food data');
        }
    })
}

function get_payments() {
    return $.ajax({
        type: 'GET',
        url: '/api/v1/payment',
        success: function (data) {
            payments = data;
        },
        fail: function () {
            console.log('failed getting payment data');
        }
    })
}

function get_products() {
    return $.ajax({
        type: 'GET',
        url: '/api/v1/user_product',
        success: function (data) {
            process_products(data);
        },
        fail: function () {
            console.log('failed getting user products');
        }
    })
}

