from django.contrib import admin
from CountMyBeer.models import *

# Register your models here.


@admin.register(Drink)
class DrinkAdmin(admin.ModelAdmin):
    pass


@admin.register(Food)
class FoodAdmin(admin.ModelAdmin):
    pass


@admin.register(UserHasProduct)
class UserHasProductAdmin(admin.ModelAdmin):
    pass


@admin.register(Payment)
class PaymentAdmin(admin.ModelAdmin):
    pass
